package br.com.itau;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import org.w3c.dom.ls.LSOutput;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO {

    public static Map<String,String>  solicitaDados() {

        Scanner scan = new Scanner(System.in);

        System.out.println("Bem vindo ao Cadastro de clientes");

        System.out.println("Digite o nome do cliente:");
        String name = scan.nextLine();

        System.out.println("Digite o CPF do cliente");
        String cpf = scan.nextLine();

        System.out.println("Digite o endereço do cliente");
        String address = scan.nextLine();

        System.out.println("Digite a idade do cliente");
        String age = scan.nextLine();

        Map<String,String> data = new HashMap<>();

        data.put("name", name);
        data.put("cpf", cpf);
        data.put("age", age);
        data.put("address",address);

        return data;
    }
}
