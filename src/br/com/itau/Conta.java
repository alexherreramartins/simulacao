package br.com.itau;

public class Conta {
    private int agencia;
    private int numero;
    private Cliente cliente;
    private double saldo;

    public Conta(Cliente cliente){

        if (cliente.validarIdade()) {
            this.agencia = (int) (Math.random() * 9999 + 1);
            this.numero = (int) (Math.random() * 999999 + 1);
            this.cliente = cliente;
            this.saldo = 0.0;

            System.out.println(this.cliente.getNome() + ", Agencia "  + this.agencia + " e Conta "+ this.numero + " criada com sucesso!");
        }else{
            System.out.println("Conta não criada. Idade nao permitida");
        }

    }

    public int getAgencia() {
        return agencia;
    }

    public void setAgencia(int agencia) {
        this.agencia = agencia;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public void depositarDinheiro(double valor){
        if (this.cliente != null) {
            this.saldo = saldo + valor;
            System.out.println("Deposito de " + valor + " feito com sucesso!");
        }else{
            System.out.println("Conta nao cadastrada para efetuar deposito.");
        }
    }

    public void sacarDinheiro(double valor){
        if (this.cliente != null) {
            this.saldo = saldo - valor;
            System.out.println("Saque de " + valor + " feito com sucesso!");
        }else{
            System.out.println("Conta nao cadastrada para efetuar saque.");
        }
    }
}
