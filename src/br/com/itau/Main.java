package br.com.itau;

import java.util.Map;

public class Main {

    public static void main(String[] args) {

        Map<String,String> dados = IO.solicitaDados();

        Cliente cliente = new Cliente (
                dados.get("name"),
                dados.get("cpf"),
                dados.get("address"),
                Integer.parseInt(dados.get("age"))
        );

        Conta conta = new Conta(cliente);

        conta.depositarDinheiro(300.00);
        conta.sacarDinheiro(100.00);

    }
}
